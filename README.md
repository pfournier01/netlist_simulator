# Netlist simulator

## -- Project by Paul Fournier --

 

### 1.  How to use it

This is fairly easy to use : simply grab the `simulator` folder provided with this project, compile the simulator by going  `cd`ing into the folder, then calling `ocamlbuild simulator.byte`  in the `simulator` folder. You can then call it with the following syntax :

``` bash
./simulator.byte [-n step_number][-im b/i/l/B][-RF rom_folder] {/your/file.net}
```

with the arguments in `[...]` being optional, and the argument in `{...}` is mandatory.

The argument `-n` is self-explanatory. If omitted, it will run *ad vitam eternam*, or until something forces its execution (user via `Ctrl+C`, end of the world, stubborn cat...).

The argument `-im` specifies the input mode to be used a bus shoud be initiallized. Four modes are currently supported :
 - `b` : the input is bit-by-bit. The masochist choice. It is only here because I originally coded it that ay and it pained me to just remove it.
 - `l` : little-endian bit-word input mode. The default mode if the option is not specified. A bus will be asked by inputing a `n` bit word in little-endian mode.
 - `B` : big-endian bit-word input mode. A bus will be asked by inputing a `n` bit word in big-endian mode.
 - `i` : integer mode. The bus will take the value of the integer, by interpreting its binary representation as a little-endian bit word.

 The argument `-RF` (not to be confused with the famous `-rf` of `rm -rf`) specifies the path to the folder containing the ROM to be used by the netlist. More specifically, if the netlist contains a ROM named `foo`, and if the ROM folder is `/my/folder/rom`, then the simulator will look at the data from the file `/my/folder/rom/foo.rom` for the data in the ROM `foo`. More details on the wiki. Note : this feature is currently not tested, and though it should work, it comes with no warranty.

Then the simulator asks for the input one wire/bus at a time, for each step, runs through the step, and outputs the results. The input and output are through the `std` channel.

### 2. How it works

The details of the implementation are on the wiki.

The idea is rather simple here. The simulator follows this simple pattern :

1. Read and parse the netlist (as provided in the documentation, `netlist.ml`, `netlist_ast.ml`, `lexer.ml`, `parser.ml`)
2. Order the netlist according to the dependencies via a topological sort (`graph.ml`, `scheduler.ml`)
3. Simulate the netlist in a sequential fashion (`simulator.ml`)
   1. Initialize the memory (currently the ROM cannot be inputted from the user)
      The memory is represented by its id. Memory is implemented via hash-tables, see part 3. for details.
   2. Take the input.
   3. Make the combinatorial computations for a cycle.
   4. Write into the registers (rising edge of the clock).
   5. Give the output.
   6. Go back to 2. if there are other steps to simulate.

### 3. Implementation, data structures, algorithms

All the algorithms here are fairly simple, there just are a lot of them combined together. Just taking a look at the `sim_eq` function reveals a lot : it is basically just a big pattern matching.

However, there are some subtleties in some aspects.

First, as the netlist initially comes unordered, we have to order it according to the dependencies. More details in the wiki.

Then, the implementation of memory requires a little trick explained in the wiki.



### 4. Limitations of the simulator, improvements paths

Currently, the simulator is a **pain** to use. There are several reasons for that.

1. The input is all manual by now, there is no way to automate a large or repeated input other than by copying and pasting it in the shell. 
2. ~~While the ROM *theoretically* is implemented and works as intended, there is not way for the user to precise its content. Therefore, it will be initialized as full of zeros and shall stay that way forever.~~
   - ~~It would be therefore desired if the user could provide the ROM contents in standalone files , written in a particular format such that the simulator can read them while the memory is initialized.~~

3. ~~There is no way to interact with the simulator automatically by providing it with files for the input or the output, which would be of much use for the final microprocessor project.~~ As mentionned by Ryan Lahfa, trough some informal discussion, it would be possible to use the input from a file by piping it through the command line. However, this is **untested**, so it should not be considered a feature for now.

### 5. Personal difficulties in this project	

The simulator project in itself was not particularly hard to do in terms of technicality or code management, despite the usual syntax errors and other "Why does this not work ? Oh wait, it takes a bad version of the scheduler in the cache, let me recompile everything properly, and now it works". The most difficult part of it, honestly, is *time and project management*.

To be fair, I'm completely new to designing big projects like these, and I tend to underestimate the amount of work required to achieve the wanted result. I also had quite a loaded timetable until now, so it was not trivial to find enough time to work on this while not neglecting the rest of the courses.

What's more, I really have a **poor** sense of organization when working on such non-trivial pieces of code. I tend to tall into the trap of having `simulator.ml`, realizing it's garbage, and then redoing it completely as `simulator2.ml`, while keeping the older file in the same folder, etc. And I was working alone on this project, with source being local on my laptop. Imagine what kind of pandemonium it would be to do group work in such conditions! On this subject, as I'm not really proficient, I would really appreciate some advice regarding version management and teamwork.