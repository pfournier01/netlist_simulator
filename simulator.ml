open Netlist_ast


(* -- execuution parameters -- *)
let print_only = ref false
let number_steps = ref (-1)
let input_mode = ref "b"
let rom_folder = ref "./ROM"

(* -- context -- *)
let var = ref Env.empty
let add k v = var := Env.add k v !var
let find k = Env.find k !var
let reg = Hashtbl.create 128
let ram = Hashtbl.create 64
let rom = Hashtbl.create 64
           
let val_arg = function
  | Avar(id) -> (try find id with Not_found -> failwith ("error val_arg : "^id))
  | Aconst(v) -> v

               
(*  -- processing --  *)

(* - auxilliary functions  - *)
               
let anot arg id = match val_arg arg with
  (* computes the output of a not gate, also works on buses *)
  | VBit(b) ->
     add id (VBit(not b))
  | VBitArray(ba) ->
     add id (VBitArray(Array.map not ba))
                 
let abinop op a1 a2 id =
  (* computes the output of a binary operator, also works on buses *)
  let op_bit x y =
    match op with
    | Or -> (x || y)
    | Xor -> ((x || y) && not(x && y))
    | And -> (x && y)
    | Nand -> (not (x&&y))
  in
  let v1 = val_arg a1 in
  let v2 = val_arg a2 in
  match (v1,v2) with
  | (VBit(x),VBit(y)) -> add id (VBit(op_bit x y))
  | (VBit(_), VBitArray(_)) | (VBitArray(_), VBit(_)) ->
     failwith "Type mismatch"
  | (VBitArray(xa), VBitArray(xb)) ->
     try
       add id (VBitArray (Array.map2 op_bit xa xb))
     with Invalid_argument _ -> failwith "Binop on two buses of different sizes"
       
let amux s a0 a1 id =
  (* computes the output of a multiplexer, does not work on buses *)
  match (val_arg s, val_arg a0, val_arg a1) with
  | (_,_,VBitArray _)
    | (_,VBitArray _, _)
    | (VBitArray _,_,_) -> failwith "Multiplexer incompatible with buses"
  | (VBit bs, VBit b0, VBit b1) -> if bs then add id (VBit b1)
                                   else add id (VBit b0)

                                 
let aconcat a1 a2 id =
  (* concatenates two wires/buses *)
  let (t1,t2) =
    match (val_arg a1, val_arg a2) with
    | (VBit(x),VBitArray(y)) -> ([|x|],y) 
    | (VBitArray(x),VBit(y)) -> (x,[|y|])
    | (VBit(x),VBit(y)) -> ([|x|], [|y|])
    | (VBitArray(x), VBitArray(y)) -> (x,y)
  in
  add id (VBitArray(Array.append t1 t2))

let aslice i1 i2 a id =
  (* slices two buses, not wires *)
  match val_arg a with
  | VBit(_) -> failwith "Nothing to slice"
  | VBitArray(x) -> begin
      let res = Array.make (i2-i1+1) false in
      for i = 0 to i2-i1 do
        res.(i) <- x.(i+i1)
      done;
      add id (VBitArray(res))
    end

let select i a id = match val_arg a with
  (* selects a wiire from a bus *)
  | VBit(x) -> (* should not happen, but ¯\_(ツ)_/¯, it's not meaningless *)
     add id (VBit(x))
  | VBitArray(x) -> add id (VBit(x.(i)))
                  
let sim_eq (id, exp) =
  (* simulates an equation
     we only read in memory here *)
  begin
    match exp with
    | Earg(arg) -> add id (val_arg arg)
    | Ereg(_) -> (try add id (Hashtbl.find reg id) with Not_found -> failwith ("No register "^id))
    | Enot(arg) -> anot arg id
    | Ebinop(op, a1, a2) -> abinop op a1 a2 id
    | Emux(s,a0,a1) -> amux s a0 a1 id
    | Erom(_, word_size, read_addr) ->
       add id
         (try
            (Hashtbl.find
               (try Hashtbl.find rom id with Not_found -> failwith ("Non existing ROM "^id))
               (val_arg read_addr))
          with Not_found -> VBitArray(Array.make word_size false))
    | Eram(_, word_size, read_addr, _, _, _) ->
       add id
         (try
            (Hashtbl.find
               (try Hashtbl.find ram id with Not_found -> failwith ("Non existing RAM "^id))
               (val_arg read_addr))
          with Not_found -> VBitArray(Array.make word_size false))
    | Econcat(a1,a2) -> aconcat a1 a2 id
    | Eslice(i1,i2, a) -> aslice i1 i2 a id
    | Eselect(i,a) -> select i a id
  end

(* -- Write in memory -- *)

let write_mem program =
  let aux eq =
    let id = fst eq in
    match snd eq with
    | Ereg(x) -> begin
        let value = (try find x with Not_found -> failwith ("Register "^id^" not connected")) in
        Hashtbl.replace reg id value
      end
    | Eram(_,word_size, _, write_enable, write_addr, data) ->
       let w_e = match val_arg write_enable with
         |VBit(x) | VBitArray([|x|]) -> x
         |VBitArray _ -> failwith ("write_enable input for RAM "^id^" is a bus")
       in
       if w_e then
         begin
           let r = Hashtbl.find ram id in (* same object in memory as the Hashtbl element, represents the values in the ram we consider *)
           Hashtbl.replace r (val_arg write_addr) (val_arg data)
         end
    | _ -> ()
  in
  List.iter aux program.p_eqs
  
(* -- Init the memory to void -- *)

let init prog =
  let aux eq =
    match eq with
    | (id,Eram(adrs,_,_,_,_,_)) -> Hashtbl.add ram id (Hashtbl.create (1 lsl (adrs/2)))
    | (id,Erom(adrs,words,_)) ->
       Hashtbl.add rom id (Hashtbl.create (1 lsl (adrs/2)));
       let r = Hashtbl.find rom id in
       let filename = (!rom_folder)^id^".rom" in
       let file = try open_in filename with Sys_error _ -> failwith ("ROM "^id^" could not be initialized : "^filename^" missing.") in
       let current = Array.make words false in
       let nil_word = Array.make words false in
       let boolarray_of_int x =
         let i = ref 1 in
         let res = Array.make adrs false in
         for j=adrs-1 downto 0 do
           res.(j) <- x mod !i = 1;
           i := !i lsl 1
         done;
         res
       in
       begin
         try
           let i = ref 0 in
           while !i < 1 lsl adrs do
             let l = input_line file in
             if String.length l > words then
               failwith ("Word "^(string_of_int !i)^"in ROM file "^filename^" has not the correct length. Got "^(string_of_int (String.length l))^ ", expected "^(string_of_int words)^". Bruh.")
             else
               begin
                 for j=0 to words-1 do
                   match l.[j] with
                   | '0' -> current.(j) <- false
                   | '1' -> current.(j) <- true
                   | _ as x -> failwith ("Word"^(string_of_int !i)^"in ROM file"^filename^" has an invalid character at position "^(string_of_int j)^". Got "^(String.make 1 x)^", expected 0 or 1")
                 done;
                 if current != nil_word then
                   (* we only add non-nil words to the ROM to gain space *)
                   Hashtbl.add r (VBitArray(boolarray_of_int !i)) (VBitArray(current));
               end
           done;         
           (* if i < lsl l n : the last words of the ROM are null *)
         with End_of_file -> () end
         close_in file
    (* Not supported yet, init to 0 *)
    | (id,Ereg(_)) -> Hashtbl.add reg id (VBit(false))
    | _ -> ()
  in
  List.iter aux prog.p_eqs
  
(* -- Input -- *)
                     
(* Variable types in p.p_vars *)
let input_wire id =
  print_string (String.concat "" ["\n";id;"="]);
  add id (VBit(read_int () != 0))

let input_bus id n =
  let res = Array.make n false in
  begin
    match !input_mode with
    | "b" -> begin
        for i = 0 to n-1 do
          print_string (String.concat "" ["\n";id;".(";string_of_int i;")="]);
          res.(i) <- (read_int () != 0)
        done
      end
    | "l" -> begin
        print_string ("\n"^(string_of_int n)^", "^id^"= ");
        let inp = read_line () in
        if String.length inp <> n then
          failwith ("Requested word of size "^(string_of_int n)^", got size "^(string_of_int (String.length inp)))
        else
          begin
            for i=0 to n-1 do
              match inp.[i] with
              | '0' -> res.(i) <- false
              | '1' -> res.(i) <- true
              | _ -> failwith ("Requested binary word, got incorrect character at location "^(string_of_int i))
            done;
          end
      end
    | "B" -> begin
        print_string ("\n"^(string_of_int n)^", "^id^"= ");
        let inp = read_line () in
        if String.length inp <> n then
          failwith ("Requested word of size "^(string_of_int n)^", got size "^(string_of_int (String.length inp)))
        else
          begin
            for i=0 to n-1 do
              match inp.[i] with
              | '0' -> res.(n-1-i) <- false
              | '1' -> res.(n-1-i) <- true
              | _ -> failwith ("Requested binary word, got incorrect character at location "^(string_of_int i))
            done;
          end
      end
    | "i" ->
       begin
         print_string("\n"^(string_of_int n)^", "^id^"= ");
         let inp = ref (read_int ()) in
         for i=n-1 downto 0 do
           let (q,r) = (!inp/2, !inp mod 2) in
           inp := q;
           res.(i) <- (r=1)
         done;
         if !inp <> 0 then
           failwith ("Requested int of size at most 2^"^(string_of_int n))
       end
    | _ as x -> failwith ("Input format \""^x^"\" not supported")
  end;
  add id (VBitArray(res))

  
let input program =
  List.iter (fun id ->
      match Env.find id program.p_vars with
      | TBit -> input_wire id
      | TBitArray(n) -> input_bus id n)
    program.p_inputs

(* -- Output -- *)

let output program =
  let out = program.p_outputs in
  let out_wire x =
    match find x with
    | VBit(v) ->
       print_string (String.concat "" [x;"=";Bool.to_string v;"\n"])
    | VBitArray _ -> assert false
  in
  let out_bus x n =
    match find x with
    | VBit _ -> assert false
    | VBitArray(v) -> begin
        let values = Array.map (Bool.to_int) v in
        let res = ref 0 in
        for i = 0 to (Array.length values -1) do
          res := 2 * !res + values.(i)
        done;
        print_string (String.concat "" [x;"=";Int.to_string !res;"\n"])
      end in
  List.iter (fun x-> match Env.find x program.p_vars with
                     | TBit -> out_wire x
                     | TBitArray(n) -> out_bus x n) out
  
  
let sim_step program =
  List.iter sim_eq program.p_eqs

let cycle program =
  var := Env.empty;
  input program;
  sim_step program;
  write_mem program;
  output program

  
let simulator program number_steps =
  init program;
  if number_steps = -1 then
    while true do
      cycle program
    done
  else
    for i=1 to number_steps do
      cycle program
    done
                 
let compile filename =
  try
    let p = Netlist.read_file filename in
    begin try
        let p = Scheduler.schedule p in
        simulator p !number_steps
      with
        | Scheduler.Combinational_cycle ->
            Format.eprintf "The netlist has a combinatory cycle.@.";
    end;
  with
    | Netlist.Parse_error s -> Format.eprintf "An error accurred: %s@." s; exit 2

let main () =
  Arg.parse
    ["-n", Arg.Set_int number_steps, "Number of steps to simulate"; "-im", Arg.Set_string input_mode, "Input mode, 'b' for bit-by-bit, 'l' for litte-endian bit-word, 'B' for big-endian bit-word, 'i' for integer"; "-RF", Arg.Set_string rom_folder, "ROM Folder"]
    compile
    ""
;;

main ()
