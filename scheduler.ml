open Netlist_ast
open Graph

exception Combinational_cycle

let read_arg = function
  | Avar(id) -> [id]
  | _ -> []
        
let read_expr = function
  | Earg(arg)
    | Enot(arg)
    | Eselect(_, arg)
    | Eslice(_, _, arg)
    | Erom(_, _, arg)
    | Eram(_, _, arg, _, _, _) (* ? *) -> read_arg arg
  | Ereg(ident) -> []
  | Ebinop(_, arg1, arg2) | Econcat(arg1, arg2) -> (read_arg arg1) @ (read_arg arg2) 
  | Emux(arg1,arg2, arg3) -> (read_arg arg1) @ (read_arg arg2) @ (read_arg arg3)
;;

let read_exp eq =
  (* Netlist_ast.equation -> Netlist_ast.ident list *)
  let (v, e) = eq in
  let tmp = v :: (read_expr e) in
  let uniq l =
    let ht = Hashtbl.create 64 in
    let rec aux = function
      | [] -> []
      | h::t -> if Hashtbl.mem ht h then aux t
                else (Hashtbl.add ht h h; h::(aux t))
    in
    aux l
  in
  uniq tmp
(* List.sort_uniq (fun x y -> if x=y then 0 else 0) tmp *);;
                
let schedule p =
  let g = mk_graph () in
  List.iter (add_node g) p.p_inputs;
  List.iter (fun (x,_) -> add_node g x) p.p_eqs;
  let aux eq = match read_exp eq with
     | [] -> ()
     | (h::t) -> begin
         (* List.iter (add_node g) (h::t); *)
         List.iter (fun x -> add_edge g h x) t
       end
  in
  (List.iter aux p.p_eqs);
  try let var = List.rev(topological g) in (* Variables triés selon leur dépendances *)
      let comp eq1 eq2 = (* Compare deux équations eq1 et eq2 telles que eq1 < eq2 ssi la variable de eq1 doit être calculée avant celle de eq2 *)
        
        let rec aux = function
          | [] -> 0
          | h::t -> if h = fst(eq1) then -1
                    else if h = fst(eq2) then 1
                    else aux t
        in
        aux var
      in
      {p with p_eqs=(List.stable_sort comp p.p_eqs)}
  with Cycle -> raise Combinational_cycle
;;
